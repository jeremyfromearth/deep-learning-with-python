import os, shutil

# Chage to your location on your filesystem
original_dataset_dir = '/Users/jeremy/Data/dogs-vs-cats/train'

def mkdir(path):
  if not os.path.isdir(path): os.mkdir(path)

# Create a directory structure for the new dataset
base_dir = './dogs-vs-cats'
mkdir(base_dir)

train_dir = os.path.join(base_dir, 'train')
mkdir(train_dir)

validation_dir = os.path.join(base_dir, 'validation')
mkdir(validation_dir)

test_dir = os.path.join(base_dir, 'test')
mkdir(test_dir)

train_cats_dir = os.path.join(train_dir, 'cats')
mkdir(train_cats_dir)

train_dogs_dir = os.path.join(train_dir, 'dogs')
mkdir(train_dogs_dir)

validation_cats_dir = os.path.join(validation_dir, 'cats')
mkdir(validation_cats_dir)

validation_dogs_dir = os.path.join(validation_dir, 'dogs')
mkdir(validation_dogs_dir)

test_cats_dir = os.path.join(test_dir, 'cats')
mkdir(test_cats_dir)

test_dogs_dir = os.path.join(test_dir, 'dogs')
mkdir(test_dogs_dir)

fnames = ['cat.{}.jpg'.format(i) for i in range(1000)]
for fname in fnames:
  src = os.path.join(original_dataset_dir, fname)
  dst = os.path.join(train_cats_dir, fname)
  shutil.copyfile(src, dst)

fnames = ['cat.{}.jpg'.format(i) for i in range(1000, 1500)]
for fname in fnames:
  src = os.path.join(original_dataset_dir, fname)
  dst = os.path.join(validation_cats_dir, fname)
  shutil.copyfile(src, dst)

fnames = ['cat.{}.jpg'.format(i) for i in range(1500, 2000)]
for fname in fnames:
  src = os.path.join(original_dataset_dir, fname)
  dst = os.path.join(test_cats_dir, fname)
  shutil.copyfile(src, dst)

fnames = ['dog.{}.jpg'.format(i) for i in range(1000)]
for fname in fnames:
  src = os.path.join(original_dataset_dir, fname)
  dst = os.path.join(train_dogs_dir, fname)
  shutil.copyfile(src, dst)

fnames = ['dog.{}.jpg'.format(i) for i in range(1000, 1500)]
for fname in fnames:
  src = os.path.join(original_dataset_dir, fname)
  dst = os.path.join(validation_dogs_dir, fname)
  shutil.copyfile(src, dst)

fnames = ['dog.{}.jpg'.format(i) for i in range(1500, 2000)]
for fname in fnames:
  src = os.path.join(original_dataset_dir, fname)
  dst = os.path.join(test_dogs_dir, fname)
  shutil.copyfile(src, dst)

