import numpy as np
from keras import models
from keras import layers
from keras.datasets import boston_housing

# Boston Home Price Predictions

## Load the training data
(train_data, train_targets), (test_data, test_targets) = boston_housing.load_data()

print('Training data shape: {}, Test data shape: {}'.format(train_data.shape, test_data.shape))

## Normalize the data by subtracting the mean and dividing by the standard deviation
mean = train_data.mean(axis=0)
train_data -= mean
stdev = train_data.std(axis=0)
train_data /= stdev
test_data -= mean
test_data /= stdev

## Build a regresssion model with one output unit
def build_model():
  model = models.Sequential()
  model.add(layers.Dense(16, activation='relu', input_shape=(train_data.shape[1],)))
  model.add(layers.Dense(16, activation='relu'))
  model.add(layers.Dense(1))
  model.compile(optimizer='rmsprop', loss='mse', metrics=['mae'])
  return model

## Use K-Fold validation on the training data
k = 4
num_val_samples = len(train_data) // k # double slash divides and floors
num_epochs = 80
all_scores = []
all_mae_histories = []

for i in range(k):
  print('Processing fold #{}'.format(i+1))
  start_idx = i * num_val_samples
  end_idx = (i+1) * num_val_samples
  val_data = train_data[start_idx : end_idx]
  val_targets = train_targets[start_idx : end_idx]

  partial_train_data = np.concatenate(
    [train_data[:start_idx], train_data[end_idx:]], axis=0)

  partial_train_targets = np.concatenate(
    [train_targets[:start_idx], train_targets[end_idx:]], axis=0)

  model = build_model()
  history = model.fit(
    partial_train_data, partial_train_targets, 
    validation_data=(val_data, val_targets), 
    epochs=num_epochs, batch_size=1, verbose=1)

  val_mse, val_mae = model.evaluate(val_data, val_targets, verbose=0)
  all_scores.append(val_mae)
  mae_history = history.history['val_mean_absolute_error']
  all_mae_histories.append(mae_history)

## Take the average of the mean absolute error
average_mae_history = [np.mean([x[i] for x in all_mae_histories]) for i in range(num_epochs)]
print(average_mae_history)

## Evaluate the model on the test data
model = build_model()
model.fit(train_data, train_targets, epochs=80, batch_size=16, verbose=1)
test_mse_score, test_mae_score = model.evaluate(test_data, test_targets)
print(test_mae_score)
