# Reuters Newswire Classification
import copy
import numpy as np
from keras import models
from keras import layers
from keras.datasets import reuters
from keras.utils.np_utils import to_categorical

(train_data, train_labels), (test_data, test_labels) = reuters.load_data(num_words=10000)
print('Number of train examples: {}, Number of test examples: {}'.format(len(train_data), len(test_data)))

## Get the word index
word_index = reuters.get_word_index()

## Convert sequences of words indices back into text
reverse_word_index = {value:key for key, value in word_index.items()}
decoded_newswire = ' '.join([reverse_word_index.get(i - 3, '?') for i in train_data[0]])

## Convert sequences of word indices to one hot encoded vectors
def vectorize_sequences(sequences, dim=10000):
  results = np.zeros((len(sequences), dim))
  for i, sequence in enumerate(sequences):
    results[i, sequence] = 1
  return results

x_train = vectorize_sequences(train_data)
x_test = vectorize_sequences(test_data)

## One hot encode the training and test labels
one_hot_train_labels = to_categorical(train_labels)
one_hot_test_labels = to_categorical(test_labels)

## Create the model
model = models.Sequential()
model.add(layers.Dense(64, activation='relu', input_shape=(10000,)))
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(46, activation='softmax'))
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

## Create a validation set for the training data
x_val = x_train[:1000]
partial_x_train = x_train[1000:]

y_val = one_hot_train_labels[:1000]
partial_y_train = one_hot_train_labels[1000:]

## Train the model
history = model.fit(
  partial_x_train, partial_y_train,
  epochs=9, batch_size=512,
  validation_data=(x_val, y_val))

results = model.evaluate(x_test, one_hot_test_labels)
print(results)

### Evaluate the accuracy of randomly classifying the articles
test_labels_copy = copy.copy(test_labels)
np.random.shuffle(test_labels_copy)
hits_array = np.array(test_labels) == np.array(test_labels_copy)
print('Accuracy of random choices', float(np.sum(hits_array)) / len(test_labels))

## Make predictions using the new model
predictions = model.predict(x_test)
print('Predicted class for first article', np.argmax(predictions[0]))
