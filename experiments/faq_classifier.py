import yaml
import numpy as np
import spacy

from keras.models import Model, Sequential
from keras.layers import TimeDistributed, Activation, Bidirectional
from keras.layers import Dense, Input, Dropout, LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.initializers import glorot_uniform
from keras.optimizers import Adam, SGD, RMSprop, Adagrad

print('Loading language model: en_core_web_lg')
nlp = spacy.load('en_core_web_lg')

def strings_to_indices(strings, max_length=None):
  """ Transforms a list of strings into their id equivalents """
  m = max_length or max(len(s) for s in strings)
  result = np.zeros((len(strings), m), dtype=int)
  for i, string in enumerate(strings):
    for j, t in enumerate(string):
      if j < m:
        result[i, j] = t.vocab.vectors.find(key=t.orth)
      else:
        break
  return result

def create_embed_layer(input_length, embeddings):
  """ Creates an embedding layer from """
  embedding_layer = Embedding(
    embeddings.shape[0], embeddings.shape[1], 
    input_length=input_length, weights=[embeddings], trainable=False)
  return embedding_layer

def one_hot(Y, n):
  n = n or len(np.unique(Y))
  return np.eye(n)[Y]

def shuffle_xy(x, y, seed=0):
  assert(len(x) == len(y))
  np.random.seed(seed)
  idx = np.linspace(0, len(x)-1, len(x), dtype='int32')
  np.random.shuffle(idx)
  x = x[idx]
  y = y[idx]
  return x, y

print('Loading data/synth-faq.yaml')
faq = yaml.load(open('data/synth-faq.yaml'))
X = np.array([nlp(q) for x in faq for q in x['questions']])
Y = np.array([i for i, x in enumerate(faq) for j, qs in enumerate(x['questions'])])
X, Y = shuffle_xy(X, Y, 0)

max_length = 16                 # Max number of words for a question to have
n_classes = len(np.unique(Y))   # How many question/answer pairs are there?

X = strings_to_indices(X, max_length)
Y = one_hot(Y, n_classes)

X_train = X[:70]
Y_train = Y[:70]
X_val = X[70:]
Y_val = Y[70:]

print('Creating embedding layer')
idx_input = Input(np.array((max_length,), dtype='int32'))
embedding_layer = create_embed_layer(max_length, nlp.vocab.vectors.data)

print('Building LSTM')
model = Sequential()
model.add(embedding_layer)
model.add(Bidirectional(LSTM(128, return_sequences=True)))
model.add(Dropout(0.5))
model.add(LSTM(64))
model.add(Dropout(0.5))
model.add(Dense(n_classes, activation='softmax'))
model.add(Activation('softmax'))
model.compile(optimizer=Adam(), loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()

print('Training model')
history = model.fit(
  X_train, Y_train,
  validation_data=(X_val, Y_val),
  batch_size=10, epochs=80,
  shuffle=False, verbose=True
)

tests = strings_to_indices([
    nlp('What is the definition of modular?'),
    nlp('How are analog and digital synthesizers the same?'),
    nlp('Tell me, how are digital and analog synths different.'),
    nlp('What is Gate and Trigger all about?'),
    nlp('What are the settings of a filter?'),
    nlp('What can you tell me about Bob Moog?'),
    nlp('What is an airplane'),
    nlp('Is there a dog that can do backflips?')
], max_length)


predictions = model.predict(tests)
print(predictions)
scores = np.max(predictions, axis=1)
answers = np.argmax(predictions, axis=1)

for x, y in zip(answers, scores):
  print('Answer idx:', x, '\nScore:', y, '\n', faq[x]['answer'], '\n')
