# IMDB Review Classification 

import numpy as np
from keras.datasets import imdb
from keras import models, layers
from rune.net import Client

## Load training and test sets 
(train_data, train_labels), (test_data, test_labels) = imdb.load_data(num_words=10000)

## Get the word index
word_index = imdb.get_word_index()

## Remap sequences of integers back into original text
reverse_word_index = dict([(v, k) for (k, v) in word_index.items()])
decoded = ' '.join([reverse_word_index.get(i - 3, '?') for i in train_data[0]])

## One-hot encode the reviews
def vectorize_seqeuences(sequences, dimension=10000):
  results = np.zeros((len(sequences), dimension))
  for i, sequence in enumerate(sequences):
    results[i, sequence] = 1
  return results

x_train = vectorize_seqeuences(train_data)
x_test = vectorize_seqeuences(test_data)

## Create numpy arrays from train and test labels
y_train = np.asarray(train_labels).astype('float32')
y_test = np.asarray(test_labels).astype('float32')

# Build the Neural Network
model = models.Sequential()
model.add(layers.Dense(16, activation='relu', input_shape=(10000, )))
model.add(layers.Dense(16, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))
model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=['accuracy'])

## Create a validation set from the original training data
x_val = x_train[:10000]
y_val = y_train[:10000]
partial_x_train = x_train[10000:]
partial_y_train = y_train[10000:]

## Train the model
history = model.fit(
  partial_x_train, partial_y_train, 
  epochs=20, batch_size=512, verbose=False,
  validation_data=(x_val, y_val))

## Test the model
results = model.evaluate(x_test, y_test)
